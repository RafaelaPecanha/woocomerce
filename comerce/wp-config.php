<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'comerce' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'v;]$! C7&u+xzmRHv )u%JeS!wh>1T]sIZSlirss%l5tbYy(Ujt)[D,{2>i=1ie,' );
define( 'SECURE_AUTH_KEY',  '0`#c&Rbvg)O63T$KffQ[=x7UM?+*F5eU^`+TRgY J.R>|KFn$wZq=ZVlit.=N,`g' );
define( 'LOGGED_IN_KEY',    'Bb^:a[Z__.SO)kY xnI.g!I_%$n<xe@pke.pWH<+ ql8rW$4:B7Fah+}AG???9hm' );
define( 'NONCE_KEY',        '^b}=9XL)=I@dDSaCyvFKW>thMY B5K@MWz@OCGf 6RNU_as( >R]$e%bw73iQ@u]' );
define( 'AUTH_SALT',        'FhF$Ko- q1>MkdOw%MEC^/>`0f8?kdjpGXvVH4(E^;bs}c<#v^j^E/M|8IBxnH;S' );
define( 'SECURE_AUTH_SALT', ',nQ N;M1^%-oq0Z/in27/aUS=0$[a;)Ux`y;s6+#4)#V?mMbML(;9kYNW@63P0y6' );
define( 'LOGGED_IN_SALT',   'e_D{PpW5edRl:Ns~YLO3T8bP<TY2[&-uWE5un:DAaE_ngs6Z8}5gHEaLPqKFbYXZ' );
define( 'NONCE_SALT',       'vq_/ G/I_Xr4O 0k88u+[0}4~%TM`k7Ft1eXZ4Q_A$%<xPS;]D9v>$L>$QmLK*TX' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
